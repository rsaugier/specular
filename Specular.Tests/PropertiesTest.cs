using System;
using System.Diagnostics;
using System.Linq;
using FluentAssertions;
using Specular.Extensions;
using Xunit;

namespace Specular.Tests
{
    public class Foo
    {
        public int Bar1 { get; } = 42;
        public string Bar2 { get; } = "fnu";
    }
    
    public class PropertiesTest
    {
        [Fact]
        public void Properties_WhenReflectingSimpleClass_ShouldReturnProperties()
        {
            var foo = new Foo();
            foo.Reflect().Properties.Should().AllBeAssignableTo<IRObjectProperty>();
            foo.Reflect().Properties.Should().BeEquivalentTo(
                new {Name = nameof(Foo.Bar1), Value = 42},
                new {Name = nameof(Foo.Bar2), Value = "fnu"});
        }
    }
}