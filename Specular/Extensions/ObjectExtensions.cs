﻿using Specular;

namespace Specular.Extensions
{
    public static class ObjectExtensions
    {
        public static IRClass ReflectClass(this object obj)
        {
            return Specular.Reflection.Reflect(obj.GetType());
        }

        public static IRObject Reflect(this object obj)
        {
            return Specular.Reflection.Reflect(obj);
        }
    }
}