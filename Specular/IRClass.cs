﻿using System;
using System.Collections.Generic;

namespace Specular
{
    public interface IRClass : IRMember, IRScope
    {
        Type Type { get; }
        IEnumerable<IRClassProperty> Properties { get; }
        IEnumerable<IRMember> Members { get; }
        IEnumerable<IRClassEvent> Events { get; }
        IEnumerable<IRClassField> Fields { get; }
        IEnumerable<IRClassMethod> Methods { get; }
        IEnumerable<IRClass> NestedClasses { get; }
    }
}
