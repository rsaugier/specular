﻿using Specular.Impl.Standard;

namespace Specular
{
    public class Specular
    {
        static Specular()
        {
        }
        
        private Specular()
        {
        }

        private static readonly ReadOnlyReflection _instance = new ReadOnlyReflection();
        public static IReadOnlyReflection Reflection => _instance;
    }
}