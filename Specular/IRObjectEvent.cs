﻿namespace Specular
{
    public interface IRObjectEvent : IRObjectMember
    {
        IRClassEvent ClassEvent { get; }
    }
}