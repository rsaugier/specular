﻿using System.Collections.Generic;

namespace Specular
{
    public interface IRObject
    {
        IEnumerable<IRObjectProperty> Properties { get; }
        IEnumerable<IRObjectMember> Members { get; }
        IEnumerable<IRObjectField> Fields { get; }
        IEnumerable<IRObjectMethod> Methods { get; }
        IEnumerable<IRClass> NestedClasses { get; }
        object ActualUntypedObject { get; }
        IRClass Class { get; }
    }
}