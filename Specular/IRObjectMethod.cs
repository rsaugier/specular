﻿namespace Specular
{
    public interface IRObjectMethod : IRObjectMember
    {
        IRClassMethod ClassMethod { get; }
    }
}