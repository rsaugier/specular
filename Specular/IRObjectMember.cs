﻿namespace Specular
{
    public interface IRObjectMember
    {
        IRObject Object { get; }
        IRMember ClassMember { get; }
    }
}