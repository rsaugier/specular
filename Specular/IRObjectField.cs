﻿namespace Specular
{
    public interface IRObjectField : IRObjectMember
    {
        IRClassField ClassField { get; }
    }
}