# WARNING

This is work in progress - not suitable for anything yet...


# what is Specular ?
A .NET standard library providing a set of abstractions over the standard .net reflection library - with performance in mind.
The name obviously comes from a joke on specular reflection.

# design choices

- Interfaces only - the public API only exposes interfaces - no concrete types
    - This improves testability of clients.
    - Official reflection API objects are *NOT* accessible through the interfaces (except for the 'System.Type' type). This means the specular API could theoretically be implemented over an other reflection infrastructure than the standard .NET one (Even if this might seems useless as a first glance...)
- Internal caches are used to speed up access to reflection. Speed is preferred over memory consumption.
- Built with portability in mind
