﻿namespace Specular
{
    public interface IRObjectProperty: IRObjectMember
    {
        IRClassProperty ClassProperty { get; }
        string Name { get; }
        object Value { get; }
    }
}