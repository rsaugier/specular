﻿namespace Specular.Impl.Standard
{
    internal class RObjectField : IRObjectField
    {
        internal RObjectField(IRObject o, IRClassField classField)
        {
            Object = o;
            ClassField = classField;
        }

        public IRObject Object { get; }
        public IRMember ClassMember => ClassField;
        public IRClassField ClassField { get; }
    }
}