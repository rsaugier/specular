﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Specular.Impl.Standard
{
    internal class RClassProperty: IRClassProperty
    {
        private readonly RClass _class;

        internal RClassProperty(RClass @class, PropertyInfo propertyInfo)
        {
            _class = @class;
            PropertyInfo = propertyInfo;
        }

        public IRScope Parent => _class;
        public string Name => PropertyInfo.Name;
        public bool IsReadOnly => !PropertyInfo.CanWrite;
        public Type PropertyType => PropertyInfo.PropertyType;
        public readonly PropertyInfo PropertyInfo;
    }
}
