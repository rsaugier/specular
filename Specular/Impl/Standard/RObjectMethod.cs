﻿namespace Specular.Impl.Standard
{
    internal class RObjectMethod : IRObjectMethod
    {
        internal RObjectMethod(IRObject o, IRClassMethod method)
        {
            Object = o;
            ClassMethod = method;
        }

        public IRObject Object { get; }
        public IRMember ClassMember => ClassMethod;
        public IRClassMethod ClassMethod { get; }
    }
}