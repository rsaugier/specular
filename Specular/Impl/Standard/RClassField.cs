﻿using System;
using System.Reflection;

namespace Specular.Impl.Standard
{
    internal class RClassField : IRClassField
    {
        private readonly RClass _class;
        private readonly FieldInfo _fieldInfo;

        internal RClassField(RClass @class, FieldInfo fieldInfo)
        {
            _class = @class;
            _fieldInfo = fieldInfo;
        }

        public IRScope Parent => _class;
        public string Name => _fieldInfo.Name;
    }
}