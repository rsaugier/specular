﻿namespace Specular.Impl.Standard
{
    internal class RObjectEvent : IRObjectEvent
    {
        internal RObjectEvent(RObject obj, IRClassEvent classEvent)
        {
            ClassEvent = classEvent;
            Object = obj;
        }

        public IRObject Object { get; }
        public IRMember ClassMember => ClassEvent;
        public IRClassEvent ClassEvent { get; }
    }
}