﻿using System;
using System.Collections.Concurrent;

namespace Specular.Impl.Standard
{
    internal class ReadOnlyReflection : IReadOnlyReflection
    {   
        private readonly ConcurrentDictionary<Type, IRClass> _classByType = new ConcurrentDictionary<Type, IRClass>();
        
        public IRClass Reflect(Type t)
        {
            return _classByType.GetOrAdd(t, type => new RClass(t));
        }

        public IRObject Reflect(object o)
        {
            return new RObject(o, Reflect(o.GetType()));
        }
    }
}