﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Specular.Impl.Standard
{
    internal class RObject : IRObject
    {
        private readonly Lazy<IEnumerable<IRObjectProperty>> _properties;
        private readonly Lazy<IEnumerable<IRObjectField>> _fields;
        private readonly Lazy<IEnumerable<IRObjectMethod>> _methods;
        private readonly Lazy<IEnumerable<IRObjectEvent>> _events;
        private readonly Lazy<IEnumerable<IRObjectMember>> _members;

        internal RObject(Object actualObject, IRClass @class)
        {
            ActualUntypedObject = actualObject;
            Class = @class;

            _properties = new Lazy<IEnumerable<IRObjectProperty>>(
                () => Class.Properties.Select(x =>
                    {
                        return (IRObjectProperty) Activator.CreateInstance(
                            typeof(RObjectProperty<,>).MakeGenericType(Class.Type, x.PropertyType),
                            (IRObject) this, (RClassProperty) x);
                    }
                ).ToArray());
            _fields = new Lazy<IEnumerable<IRObjectField>>(
                () => Class.Fields.Select(x => new RObjectField(this, x)).ToArray());
            _methods = new Lazy<IEnumerable<IRObjectMethod>>(
                () => Class.Methods.Select(x => new RObjectMethod(this, x)).ToArray());
            _events = new Lazy<IEnumerable<IRObjectEvent>>(
                () => Class.Events.Select(x => new RObjectEvent(this, x)).ToArray());
            _members = new Lazy<IEnumerable<IRObjectMember>>(
                () => (Properties.Concat(
                    Methods.Concat(
                        Events.Concat(
                            (IEnumerable<IRObjectMember>) Fields)))).ToArray());
        }

        public object ActualUntypedObject { get; }
        public IRClass Class { get; }
        public IEnumerable<IRObjectProperty> Properties => _properties.Value;
        public IEnumerable<IRObjectMember> Members => _members.Value;
        public IEnumerable<IRObjectField> Fields => _fields.Value;
        public IEnumerable<IRObjectMethod> Methods => _methods.Value;
        public IEnumerable<IRObjectEvent> Events => _events.Value;
        public IEnumerable<IRClass> NestedClasses => Class.NestedClasses;
    }
}