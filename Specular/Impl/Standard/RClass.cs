﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Specular.Impl.Standard
{
    internal sealed class RClass : IRClass
    {
        private readonly IRScope _parent;
        private readonly Type _type;

        private readonly Lazy<IEnumerable<IRClassProperty>> _properties;
        private readonly Lazy<IEnumerable<IRClassField>> _fields;
        private readonly Lazy<IEnumerable<IRClassMethod>> _methods;
        private readonly Lazy<IEnumerable<IRClassEvent>> _events;
        private readonly Lazy<IEnumerable<IRMember>> _members;
        private readonly Lazy<IEnumerable<IRClass>> _nestedClasses;

        public RClass(Type type):
            this(null, type)
        { 
        }

        private RClass(IRScope parent, Type nestedType)
        {
            _parent = parent;
            _type = nestedType;

            _properties = new Lazy<IEnumerable<IRClassProperty>>(
                () => _type.GetProperties().Select(x => new RClassProperty(this, x)).ToArray());
            _fields = new Lazy<IEnumerable<IRClassField>>(
                () => _type.GetFields().Select(x => new RClassField(this, x)).ToArray());
            _methods = new Lazy<IEnumerable<IRClassMethod>>(
                () => _type.GetMethods().Select(x => new RClassMethod(this, x)).ToArray());
            _events = new Lazy<IEnumerable<IRClassEvent>>(
                () => _type.GetEvents().Select(x => new RClassEvent(this, x)).ToArray());
            _nestedClasses = new Lazy<IEnumerable<IRClass>>(
                () => _type.GetNestedTypes().Select(x => new RClass(this, x)));
            _members = new Lazy<IEnumerable<IRMember>>(
                () =>
                {
                    IEnumerable<IRMember> members =
                        Properties.Concat(
                            Methods.Concat(
                                Events.Concat(
                                    NestedClasses.Concat(
                                        (IEnumerable<IRMember>) Fields))));
                    return members.ToArray();
                }
            );
        }

        public Type Type => _type;
        public string Name => _type.Name;
        public IRScope Parent => _parent;
        public IEnumerable<IRClassProperty> Properties => _properties.Value;
        public IEnumerable<IRMember> Members => _members.Value;
        public IEnumerable<IRClassEvent> Events => _events.Value;
        public IEnumerable<IRClassField> Fields => _fields.Value;
        public IEnumerable<IRClassMethod> Methods => _methods.Value;
        public IEnumerable<IRClass> NestedClasses => _nestedClasses.Value;
    }
}