﻿namespace Specular.Impl.Standard
{
    internal class RObjectProperty<TClassType, TPropType> : IRObjectProperty
    {
        private delegate TPropType ReaderDelegateType(TClassType o);

        private readonly RClassProperty _classProperty;
        private readonly ReaderDelegateType _readerDelegate;

        public RObjectProperty(IRObject obj, RClassProperty classProperty)
        {
            _classProperty = classProperty;
            Object = obj;
            _readerDelegate = (ReaderDelegateType) _classProperty.PropertyInfo.GetGetMethod(true)
                .CreateDelegate(typeof(ReaderDelegateType));
        }

        public IRObject Object { get; }
        public IRMember ClassMember => _classProperty;
        public IRClassProperty ClassProperty => _classProperty;
        public string Name => _classProperty.Name;
        public TPropType TypedValue => _readerDelegate.Invoke((TClassType)Object.ActualUntypedObject);
        public object Value => TypedValue;
    }
}