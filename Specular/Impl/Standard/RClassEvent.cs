﻿using System.Reflection;

namespace Specular.Impl.Standard
{
    internal class RClassEvent : IRClassEvent
    {
        private readonly RClass _class;
        private readonly EventInfo _eventInfo;

        internal RClassEvent(RClass @class, EventInfo eventInfo)
        {
            _class = @class;
            _eventInfo = eventInfo;
        }

        public IRScope Parent => _class;
        public string Name => _eventInfo.Name;
    }
}