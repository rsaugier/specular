﻿using System;
using System.Reflection;

namespace Specular.Impl.Standard
{
    internal class RClassMethod : IRClassMethod
    {
        private readonly RClass _class;
        private readonly MethodInfo _methodInfo;

        internal RClassMethod(RClass @class, MethodInfo methodInfo)
        {
            _class = @class;
            _methodInfo = methodInfo;
        }

        public IRScope Parent => _class;
        public string Name => _methodInfo.Name;
    }
}