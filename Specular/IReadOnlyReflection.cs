﻿using System;

namespace Specular
{
    public interface IReadOnlyReflection
    {
        IRClass Reflect(Type t);
        IRObject Reflect(object o);
    }
}
