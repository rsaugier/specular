﻿namespace Specular
{
    public interface IRMember
    {
        IRScope Parent { get; }
        string Name { get; }
    }
}
