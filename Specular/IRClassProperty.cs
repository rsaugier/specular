﻿using System;

namespace Specular
{
    public interface IRClassProperty: IRMember
    {
        bool IsReadOnly { get; }
        Type PropertyType { get; }
    }
}